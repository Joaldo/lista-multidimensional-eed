package encadeada.lista;

import java.util.Scanner;

public class Executor {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Lista lista = new Lista();
		lista.qtd = 0;

		listar(lista);

		adicionarObjeto(lista);
		adicionarObjeto(lista);
		adicionarObjeto(lista);
		adicionarObjeto(lista);
		adicionarObjeto(lista);
		
		listar(lista);

		pesquisar(lista);

		// System.out.println("Primeiro elemento " + lista.primeiro.num);
		// System.out.println("Ultimo elemento " + lista.ultimo.num);

	}
	
	public static void pesquisar(Lista lista) {
		System.out.println("Qual elemento deseja encontrar? ");
		int numero = sc.nextInt();
		Elemento auxiliar = lista.primeiro;
		while(auxiliar != null && auxiliar.num != numero) {
			auxiliar = auxiliar.proximo;
		}
		if(auxiliar == null) {
			System.out.println("Elemento n�o existe.");
		}else {
			System.out.println("Elemento encontrado " + auxiliar.num);
		}
	}

	public static void listar(Lista lista) {
		Elemento auxiliar = lista.primeiro;
		if (auxiliar == null) {
			System.out.println("Lista Vazia!");
		} else {
			while (auxiliar != null) {
				System.out.println(auxiliar.num);
				auxiliar = auxiliar.proximo;
			}
		}

	}

	public static void adicionarObjeto(Lista lista) {
		Elemento novoObjeto = new Elemento();
		Elemento objAuxiliar = new Elemento();

		if (lista.qtd == 0) {
			System.out.println("Informe o numero desejado");
			novoObjeto.num = sc.nextInt();
			novoObjeto.proximo = null;

			lista.primeiro = novoObjeto;
			lista.ultimo = novoObjeto;
			lista.qtd++;

		} else {

			objAuxiliar = lista.primeiro;

			while (objAuxiliar.proximo != null) {
				objAuxiliar = objAuxiliar.proximo;
			}

			System.out.println("Informe o numero desejado");
			novoObjeto.num = sc.nextInt();
			novoObjeto.proximo = null;

			objAuxiliar.proximo = novoObjeto;

			lista.ultimo = novoObjeto;
			lista.qtd++;

		}
	}

}
