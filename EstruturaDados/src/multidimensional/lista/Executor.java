package multidimensional.lista;

import java.util.Scanner;

public class Executor {

	public static void main(String[] args) {
		inicio(new ListaMultidimensional());

	}

	static int codCat = 1;
	static int codProd = 1;

	static Scanner sc = new Scanner(System.in);
	private static Produto p;

	public static void inicio(IListaMultidimencional lista) {
		executar(lista);
		Venda venda = null;
		int menu;
		for (int i = 0; i < 1;) {
			System.out.println("Bem-Vindo! \nO que deseja fazer?" + "\n1 - Cadastrar Categoria."
					+ "\n2 - Cadastrar produto" + "\n3 - Listar" + "\n4 - Vender" + "\n5 - Sair");

			menu = sc.nextInt();
			sc.nextLine();
			switch (menu) {
			case 1:
				cadastrarCat(lista);
				break;

			case 2:
				cadastrarPro(lista);
				break;

			case 3:
				listar(lista);
				break;

			case 4:
				venda = vender(lista);
				i++;
				break;

			case 5:
				System.out.println("Volte sempre!");
				break;

			default:
				System.out.println("Opcao invalida");
				break;
			}
		}
		System.out.println("Relat�rio de venda:");
		venda.relatorioVenda.imprimir();

		System.out.println("Estoque da Executor:");
		lista.imprimir();

	}

	private static Venda vender(IListaMultidimencional lista) {
		int continuarVenda = 1;
		Venda venda = new Venda();
		while (continuarVenda == 1) {
			lista.imprimir();

			System.out.println("C�digo categoria:");
			int codC = sc.nextInt();
			Categoria categoria = lista.buscar(codC);

			System.out.println();
			System.out.println("C�digo do produto:");
			int codP = sc.nextInt();
			Produto produto = lista.buscarProduto(codC, codP);

			System.out.println("Voc� escolheu o produto " + produto.descricao + ". \nDispon�vel no estoque: "
					+ produto.quantidade + " unidades");

			// do while para ler a quantidade de produtos q o usu�rio quer comprar
			// a quantidade s� ser� aceita quando for menor ou igual que o estoque
			int quantidade;
			do {
				System.out.println("Quantidade que deseja comprar:");
				quantidade = sc.nextInt();
				if (quantidade > produto.quantidade) {
					System.out.println("N�o temos todo esse estoque!");
				}
			} while (quantidade > produto.quantidade);

			double valorFinal = produto.preco * quantidade;

			// Descri��o do pedido, ele podera aceitar ou n�o
			System.out.println("Descri��o da compra:");
			System.out.println("Produto: " + produto.descricao);
			System.out.println("Quantidade: " + quantidade);
			System.out.println("Pre�o por unidade: R$" + produto.preco);
			System.out.println("Valor total: R$" + valorFinal);
			// produto.quantidade = produto.quantidade-quantidade;
			System.out.println();
			System.out.println("Confirmar comprar? (1-sim, 2-n�o)");
			int confirmarCompra = sc.nextInt();

			// Se ele tiver aceito o pedido, ser� adicionado � lista multidimensional
			// "venda"
			// o produto que ele comprou e ser� descontado do estoque da Executor a quantidade q
			// ele comprou
			if (confirmarCompra == 1) {
				if (venda.relatorioVenda.buscar(codC) == null) {
					// � necess�rio verificar se ele j� comprou um produto daquela categoria antes
					// evitando que o registro fique duplicado
					venda.relatorioVenda.inserirCategoria(codC, categoria.descricao);
				}

				Produto produtoVenda = venda.relatorioVenda.buscarProduto(codC, codP);
				if (produtoVenda == null) {
					// o mesmo deve ser feito com produto
					venda.relatorioVenda.inserirProduto(codC, codP, produto.descricao, produto.preco, quantidade);
				} else {
					// se ele j� tiver comprado alguma quantidade daquele produto antes
					// basta adicionar ao relat�rio de venda a quantidade q ele comprou
					// e descontar do estoque da Executor
					System.out.println("PRODUTOVENDA.QUANTIDADE = " + produtoVenda.quantidade);
					produtoVenda.quantidade = produtoVenda.quantidade + quantidade;
				}

				produto.quantidade = produto.quantidade - quantidade;
				System.out.println("Compra efetuada com sucesso");
				System.out.println();

			} else {
				System.out.println("Compra descartada");
			}

			System.out.println("Deseja comprar mais alguma coisa?(1-sim, 2-n�o)");
			continuarVenda = sc.nextInt();

		}
		return venda;

	}

	private static void listar(IListaMultidimencional lista) {
		lista.imprimir();
		p = lista.buscarProduto(4, 0);

	}

	private static void cadastrarPro(IListaMultidimencional lista) {
		int codCat;
		String descricao;
		Double valor;
		int qtd;

		System.out.println("Informe o codigo da categoria");
		codCat = sc.nextInt();
		sc.nextLine();

		System.out.println("Informe a descricao do produto");
		descricao = sc.nextLine();

		System.out.println("Informe o valor do produto");
		valor = sc.nextDouble();
		sc.nextLine();

		System.out.println("Informe a quantidade do produto");
		qtd = sc.nextInt();
		sc.nextLine();
		lista.inserirProduto(codCat, codProd, descricao, valor, qtd);
		codProd++;

	}

	private static void cadastrarCat(IListaMultidimencional lista) {
		String descricao;
		System.out.println("Informe a descricao da categoria");
		descricao = sc.nextLine();
		lista.inserirCategoria(codCat, descricao);
		codCat++;

	}

	public static void executar(IListaMultidimencional lista) {
		

		lista.inserirCategoria(0, "Limpeza");
		// lista.imprimirCategoria();
		
		lista.inserirCategoria(1, "Alimento");

		lista.inserirCategoria(2, "Automotivo");

		lista.inserirCategoria(3, "Bebidas");
		
		lista.inserirCategoria(4, "Computador");
	

		// PRODUTOS
		// Categoria 0 - LIMPEZA

		lista.inserirProduto(0, 0, "Vassoura", 3.0, 50);
		

		lista.inserirProduto(0, 1, "Rodo", 3.5, 25);
		

		lista.inserirProduto(0, 2, "Pano", 5.0, 500);


		// Categoria 1 - ALIMENTO

		lista.inserirProduto(1, 0, "Arroz", 3.0, 700);
		// lista.imprimirProduto(1);

		lista.inserirProduto(1, 1, "Feij�o", 5.75, 1000);
		

		lista.inserirProduto(1, 2, "Macarr�o", 2.65, 500);
		
		lista.inserirProduto(1, 3, "Bife", 12.50, 80);
		

		

		// Categoria 2 - HIGIENE PESSOAL 

		lista.inserirProduto(2, 0, "Creme dental", 8.0, 100);
		// lista.imprimirProduto(2);

		lista.inserirProduto(2, 1, "Desodorante", 15.0, 50);
	

		lista.inserirProduto(2, 2, "Escova de dente", 5.0, 200);
		

		// Categoria 3 - BEBIDAS

		lista.inserirProduto(3, 0, "Agua", 2.65, 6000);
		// lista.imprimirProduto(3);

		lista.inserirProduto(3, 1, "Refrigerante", 6.65, 7000);

		lista.inserirProduto(3, 2, "Suco", 5.25, 650);
		
		
		// Categoria 4 - Eletronicos
		
		lista.inserirProduto(4, 0, "Notebook", 4.500, 35);
		
		lista.inserirProduto(4, 1, "Smartphone", 1.500, 300);

		lista.inserirProduto(4, 2, "TV", 2.150, 90);

		lista.inserirProduto(4, 3, "Multifuncional", 500.0, 100);


	}

}
