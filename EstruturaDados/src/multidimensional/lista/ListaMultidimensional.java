package multidimensional.lista;

public class ListaMultidimensional implements IListaMultidimencional {

	Categoria inicioCategoria = null;

	public Categoria inserirCategoria(int codigo, String descricao) {
		Categoria novo = new Categoria();
		novo.codigo = codigo;
		novo.descricao = descricao;

		if (inicioCategoria == null) {
			inicioCategoria = novo;
		} else {
			Categoria aux = inicioCategoria;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}

		return novo;
	}

	public void removerCategoria(int codigo) {
		if (inicioCategoria == null) {
			System.out.println("Lista vazia!");
		} else {
			Categoria ant = null;
			Categoria aux = inicioCategoria;
			while (aux.prox != null && aux.codigo != codigo) {
				ant = aux;
				aux = aux.prox;
			}
			if (aux.codigo == codigo) {
				if (ant == null) {
					inicioCategoria = inicioCategoria.prox;
				} else if (aux.prox == null) {
					ant.prox = null;
				} else {
					ant.prox = aux.prox;
				}
			}
		}

	}

	public Categoria buscar(int codigo) {
		if (inicioCategoria == null) {
			System.out.println("Lista vazia!");
		} else {
			Categoria aux = inicioCategoria;
			while (aux.prox != null && aux.codigo != codigo) {
				aux = aux.prox;
			}
			if (aux.codigo == codigo) {
				return aux;
			}
		}
		return null;
	}

	public void imprimirCategoria() {
		if (inicioCategoria == null) {
			System.out.println("Lista de categoria vazia!");
		} else {
			Categoria aux = inicioCategoria;
			while (aux != null) {
				System.out.print(aux.codigo + " " + aux.descricao + " ");
				aux = aux.prox;
			}
		}
		System.out.println("");

	}

	public Produto inserirProduto(int codCat, int codigo, String descricao, double preco, int quantidade) {

		Produto novo = new Produto();
		novo.codigo = codigo;
		novo.descricao = descricao;
		novo.preco = preco;
		novo.quantidade = quantidade;

		if (inicioCategoria == null) {
			System.out.println("N�o existem categorias");
		} else {
			Categoria cat = buscar(codCat);
			if (cat == null) {
				System.out.println("Categoria n�o encontrada");
			} else {
				if (cat.inicio == null) {
					cat.inicio = novo;
				} else {
					Produto aux = cat.inicio;
					while (aux.prox != null) {
						aux = aux.prox;
					}
					aux.prox = novo;
				}
				return novo;
			}
		}

		return null;
	}

	public void removerProduto(int codCat, int codigo) {
		if (inicioCategoria == null) {
			System.out.println("Lista de Produto vazia!");
		} else {
			Categoria cat = buscar(codCat);
			if (cat == null) {
				System.out.println("Categoria n�o encontrada");
			} else {
				Produto ant = null;
				Produto aux = cat.inicio;
				while (aux.prox != null && aux.codigo != codigo) {
					ant = aux;
					aux = aux.prox;
				}
				if (aux.codigo == codigo) {
					if (ant == null) {
						cat.inicio = cat.inicio.prox;
					} else if (aux.prox == null) {
						ant.prox = null;
					} else {
						ant.prox = aux.prox;
					}
				}
			}

		}

	}

	public Produto buscarProduto(int codCat, int codigo) {
		if (inicioCategoria == null) {
			System.out.println("Lista vazia!");
		} else {
			Categoria cat = buscar(codCat);

			if (cat == null) {
				System.out.println("Categoria n�o encontrada");
			} else {
				Produto aux = cat.inicio;
				while (aux.prox != null && aux.codigo != codigo) {
					aux = aux.prox;
				}
				if (aux.codigo == codigo) {
					return aux;
				}

			}

		}
		return null;
	}

	public void imprimirProduto(int codCat) {
		if (inicioCategoria == null) {
			System.out.println("Lista de produto vazia!");
		} else {
			Categoria cat = buscar(codCat);

			if (cat == null) {
				System.out.println("Categoria n�o encontrada");
			} else {
				Produto aux = cat.inicio;
				while (aux != null) {
					System.out.println("Item: " + aux.codigo + " " + aux.descricao + " - Pre�o R$:" + aux.preco);
					aux = aux.prox;
				}
			}

		}
		System.out.println("");

	}

	public void imprimir() {
		if (inicioCategoria == null) {
			System.out.println("Lista vazia!");
		} else {
			Categoria aux = inicioCategoria;
			while (aux != null) {
				System.out.print("Categoria " + aux.codigo + " - " + aux.descricao + " \n");
				imprimirProduto(aux.codigo);
				aux = aux.prox;
				System.out.println();
			}
		}
		System.out.println("");

	}

}
